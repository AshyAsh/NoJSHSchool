---
title: Home
layout: page
---

# Welcome to {{ site.title }}

Hello, I am {{ site.author }}.

So you hate JavaScript? Me too! Welcome to a lightweight and JavaScript-free
instance of {{ site.title }}

# Available courses

<ul>
  {% for post in site.posts %}
    {% assign course = site.data.courses[post.course] %}
    <li>
      <a href="{{ course.url }}">{{ course.title }}</a>
    </li>
  {% endfor %}
</ul>
