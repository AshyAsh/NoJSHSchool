---
title: Blog
layout: page
---

# Blog

{% for entry in site.data.blog.entries %}
  <a href="{{ entry.url }}">{{ entry.title }}</a>
{% endfor %}
